# Project Kuber Otus

Поднятие кластера Kubernetes в YandexCloud осуществляется Terraform v1.0.2.
Код для поднятия кластера содержится в [отдельном репозитории](https://gitlab.com/mzabolotnov/k8s_terraform.git) https://gitlab.com/mzabolotnov/k8s_terraform.git
Для поднятия кластера k8s используется pipline. Terraform-state хранится в Gitlab

### Система логирования, мониторинг, деплоя приложения

В данном репозитории хранится код для поднятия системы мониторинга и логирования кластера k8s.
Создан pipelines для создания и удаления систем.

#### Подключение к кластеру
В настройка GitLab CI/CD задано две переменные $KUBE_URL и $KUBE_TOKEN

- `KUBE_URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')`

Далее создаем сервис-аккаунт gitlab в кластере

- `kubectl apply -f gitlab/gitlab-admin-service-account.yaml`

Определяем токен доступа

- `KUBE_TOKEN=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') | grep token: | awk '{print $2}')`

Далее подключаемся

- `kubectl config set-cluster k8s --server="$KUBE_URL" --insecure-skip-tls-verify=true`
- `kubectl config set-credentials admin --token="$KUBE_TOKEN"`
- `kubectl config set-context default --cluster=k8s --user=admin`
- `kubectl config use-context default`

#### Разворачивание Prometheus, Grafana, Loki
Добавляем репозиторий для установки Loki
```
helm repo add grafana https://grafana.github.io/helm-charts
```
Добавляем репозиторий для установки Prometheus и Grafana
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
```
Устанавливаем Prometheus, Grafana, Loki
````
kubectl create ns monitoring

helm upgrade --install prometheus-stack prometheus-community/kube-prometheus-stack -n monitoring -f monitoring/custom_values.yaml

helm upgrade --install loki grafana/loki-stack  -f logging/loki-stack_values.yaml -n monitoring
````

пароль от Grafana по умолчанию
user: admin
password: prom-operator

файл monitoring/custom_values.yaml добавляет ендпоинты для Prometрeus, все поды из namespase sock-shop.

Dashboard Grafana для мониторинга кластера
- monitoring/grafana

#### Разворачивание ArgoCD

````
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/core-install.yaml
````
пароль для доступа к ArgoCD
user: admin
password: kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo

Взято [отсюда](https://argo-cd.readthedocs.io/en/stable/getting_started/) https://argo-cd.readthedocs.io/en/stable/getting_started/

### Deploy приложения

В качестве примера деплоя приложения берем [Socks-Shop](https://github.com/microservices-demo/microservices-demo.git) https://github.com/microservices-demo/microservices-demo.git

Приложение разворачиваем при помощи ArgoCD из [папки](https://github.com/microservices-demo/microservices-demo/tree/master/deploy/kubernetes/helm-chart) https://github.com/microservices-demo/microservices-demo/tree/master/deploy/kubernetes/helm-chart

Owners of projects: @mzabolotnov and @s.zolotov92